<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/api/object-create','\App\Http\Controllers\ApiController@routePostObjectCreate');
Route::get('/api/object-get','\App\Http\Controllers\ApiController@routeGetObjects');

Route::get('/{uri}', function () {
    return view('index');
})->where('uri','^.*')->name('app');

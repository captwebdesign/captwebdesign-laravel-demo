<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MdlObject;

class ApiController extends Controller
{
    public function routeGetObjects(Request $request) {
        $return = [
            'error' => false,
        ];

        $objects = MdlObject::all();

        $return['objects'] = $objects;

        return $return;
    }// routeGetObjects()
    public function routePostObjectCreate(Request $request) {

        $return = [
            'error' => false,
        ];

        $object = new MdlObject();

        $object->name = $request->input('name');
        $object->value = $request->input('value');

        $object->save();

        $return['object'] = $object;

        return $return;
    }
}

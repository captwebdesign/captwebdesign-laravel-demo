/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// Disable Warnings
Vue.config.productionTip = false;
Vue.config.devtools = false;

import Vue from 'vue';

import BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue);

import Vuex from 'vuex';

Vue.use(Vuex);

import { mapState } from 'vuex';
window.mapState = mapState;

import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [{
        path: '/',
        component: require('./routes/Home.vue').default
    },
    {
        path: '/demo-orm',
        component: require('./routes/DemoOrm.vue').default
    },
    {
        path: '/demo-vuejs',
        component: require('./routes/DemoVuejs.vue').default
    },
    {
        path: '/about',
        component: require('./routes/About.vue').default
    },
];

const router = new VueRouter({
    mode: 'history',
    routes
});



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./components', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const store = new Vuex.Store({
    state: {
        objects: []
    }, // state
    mutations: {
        objects(state, data) {
            state.objects = data.value;
        },// objects
    }, // mutations
    actions: {
        loadObjects(context, data) {
            console.log('loading objects');
            var _this = this;
            axios.get('/api/object-get')
            .then(function(response) {
                console.log(response);
                if(response.data.error) {
                    // error
                    console.log('Error loading objects.');
                    return;
                }
                context.commit('objects',{ value: response.data.objects });
            })// .then()
            .catch(function(e) {
                console.log('Error loading objects.');
            });// .catch()
        },// loadObjects()
    },// actions
});

const app = new Vue({
    el: '#app',
    store,
    router,
    mounted() {
        this.$store.dispatch('loadObjects');
    }
});

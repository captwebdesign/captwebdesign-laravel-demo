# CaptWebDesign Laravel Demo

## Getting Started

1. Install by using composer.  
    `composer install`

2. Create a new MySQL database and set the necessary config values in the .env file.

3. Run migrate  
  `php artisan migrate`

4. Start the local webserver  
  `php artisan serve`

5. Now open your web browser and go to: `http://localhost:8000`

## License

**CaptWebDesign Laravel Demo**  
Sorry, no licensing available at this time.

**Laravel Framework**  
The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
